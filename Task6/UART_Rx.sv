// Project: UART Rx
// Module:  UART_Rx
// Author:  María Luisa Ortiz Carstensen
// Date:    23 febrero 2023

module UART_Rx #(parameter DW = 8)
(
	input		clk,
	input		enb,
	input		rx_i,
	input		rst,
	
	output	[DW-1:0]rx_o,
	output	rx_ready
);

wire ovf8_l;
wire ovf5_l; 
wire sync_rst_l;
wire rx_l;
wire rx_done_l;
wire data_l;
wire [DW-5:0] count_5_l;
wire [DW-5:0] count_8_l;
wire [DW-1:0] rx_out_l;
wire parity_o_w;

FSM_UART fsm
(
	.clk(clk),
	.rst(rst), 
	.ovf8(ovf8_l),
	.ovf5(ovf5_l),
	.enb(enb),
	.rx(rx_i), 
	.parity(parity_o_w),
	.count5_i(count_5_l),
	.count8_i(count_8_l),
	
	.rx_done(rx_done_l),
	.rx_out(rx_l),
	.data(data_l),	
	.syncrst(sync_rst_l)
);

Counter_8 #(.DW(4)) c8
(
	.clk(clk),
	.enb(enb),
	.rst(rst),
	.sync_rst(sync_rst_l),
	.count5_i(count_5_l),
	
	.count8_o(count_8_l), 
	.ovf8(ovf8_l),
	.parity_o(parity_o_w)
);

Counter_5 #(.DW(4)) c5
(
	.clk(clk), 
	.enb(enb),
	.rst(rst),
	.sync_rst(sync_rst_l),
	
	.count5_o(count_5_l),
	.ovf5(ovf5_l)
);

SIPO #(.DW(8)) reg_out

(
	.clk(clk),
	.rst(rst),
	.enb(enb && data_l && (count_5_l == 2)),
	.inp(rx_i),
	
	.out(rx_out_l)
);

assign rx_ready = rx_done_l;
assign rx_o		 = rx_out_l;

endmodule 