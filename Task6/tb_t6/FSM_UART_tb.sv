

module FSM_UART_tb;


// Inputs
logic clk;
logic rst;
logic enb;
logic rx; 
logic syncrst;
logic parity;
logic ovf8;
logic ovf5;
logic [2:0] count5_i;
logic [3:0] count8_i;

// Outputs
logic rx_done;
logic data;
logic rx_out;

// Instantiate the FSM module
FSM_UART #(.DW(8)) uut (
	//inputs
  .clk(clk),
  .rst(rst),
  .ovf8(ovf8),
  .ovf5(ovf5),
  .enb(enb),
  .rx(rx),
  .syncrst(syncrst),
  .parity(parity),
  .count5_i(count5_i),
  .count8_i(count8_i),
  
  //outputs
  .rx_done(rx_done),
  .data(data),
  .rx_out(rx_out)
);


// Initialize signals, stimulation
initial begin
	clk = 0;
	rst = 0;
	enb = 0;
	syncrst = 0;
	count5_i = 0;
	count8_i = 0;
	parity = 0;
	ovf8 = 0;
	ovf5 = 0; 
	rx = 1; 

	  // Toggle reset signal
	#2 rst = 0;
	#2 rst = 1;
	#2 enb = 1;
		rx  = 0; // START BIT
	 count5_i = 2'b10;
		count8_i = 4'b0001; 
	#2 rx = 1;   // data [0]
		count5_i = 2'b10;
		count8_i = 4'b0010; 
	#2 rx = 0;   // data [1]
		count5_i = 2'b10;
		count8_i = 4'b0011;
	#2 rx = 1;   // data [2]
		count5_i = 2'b10;
		count8_i = 4'b0100;
	#2 rx = 0;   // data [3]
		count5_i = 2'b10;
		count8_i = 4'b0101;
	#2 rx = 1;   // data [4]
		count5_i = 2'b10;
		count8_i = 4'b0110;
	#2 rx = 0;   // data [5]
		count5_i = 2'b10;
		count8_i = 4'b0111;
	#2 rx = 1;   // data [6]
		count5_i = 2'b10;
		count8_i = 4'b1000;   		
	#2 rx = 0;   // data [7]
		count5_i = 2'b10;
		count8_i = 4'b1001; //DATA DONE
		
	#2	ovf8	= 1; //PARITY
		rx		= 1;
	
	#2 parity	= 1; //STOP
		rx			= 1;
	
	#2 count8_i = 4'b1011;
		
		

	  // Run simulation for certain duration
	  #400;

	  // Stop simulation
	  $stop;
end

 // Generate clock signal
always begin
    #1 clk = ~clk;
end


endmodule
