

module UART_rx_tb;

  
  // Inputs
  logic clk;
  logic enb;
  logic rx_i;
  logic rst;
  
  // Outputs
  logic [7:0] rx_o;
  logic rx_ready;
  
  // Instantiate the UART_Rx module
  UART_Rx #(.DW(8)) uut (
    .clk(clk),
    .enb(enb),
    .rx_i(rx_i),
    .rst(rst),
    .rx_o(rx_o),
    .rx_ready(rx_ready)
  );

  // Initialize signals, stimulation
  initial begin
    clk = 0;
    enb = 0;
    rx_i = 1;
    rst = 1;

    // Toggle reset signal
    #2 rst = 0;//active on zero
	#2 rst = 1;

    // Enable UART reception
    #2 enb = 1;

    // Start transmitting some data
     #10 rx_i = 0; // Start bit
    #10 rx_i = 1; // Data bit 0
    #10 rx_i = 1; // Data bit 1
    #10 rx_i = 0; // Data bit 2
    #10 rx_i = 1; // Data bit 3
    #10 rx_i = 0; // Data bit 4
    #10 rx_i = 1; // Data bit 5
    #10 rx_i = 0; // Data bit 6
    #10 rx_i = 0; // Data bit 7
    #10 rx_i = 0; // Parity
    #10 rx_i = 1; // Stop
     #10 rx_i = 0; // Start bit
    #10 rx_i = 1; // Data bit 0
    #10 rx_i = 1; // Data bit 1
    #10 rx_i = 0; // Data bit 2
    #10 rx_i = 1; // Data bit 3
    #10 rx_i = 0; // Data bit 4
    #10 rx_i = 1; // Data bit 5
    #10 rx_i = 0; // Data bit 6
    #10 rx_i = 0; // Data bit 7
    #10 rx_i = 0; // Parity
    #10 rx_i = 1; // Stop
	


    // Stop simulation after some time
    #1000 $stop;
  end

 // Generate clock signal
always begin
    #1 clk = ~clk;
end

endmodule
