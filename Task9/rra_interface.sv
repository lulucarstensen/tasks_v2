// Project: Task 9 - Arbiters
// Module:  Interface - Round Robin Arbiter
// Author:  María Luisa Ortiz Carstensen
// Date:    May 3rd, 2024 


interface rra_interface (input bit clk);

	logic [3 : 0] reqs;
	logic [3 : 0] grant;
	
	modport PROGRAM (input reqs, output grant);
	modport SIMULATION (output reqs, input grant);

endinterface