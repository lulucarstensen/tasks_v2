// Project: Task 9 - Arbiters
// Module:  Test Bench - Round Robin Arbiter
// Author:  María Luisa Ortiz Carstensen
// Date:    May 3rd, 2024 


`timescale 1ns / 1ps

// Project: Task 9 - Arbiters
// Module:  Test Bench - Round Robin Arbiter
// Author:  María Luisa Ortiz Carstensen
// Date:    May 3rd, 2024 

include "rra_class.svh";

module RR_Arbiter_tb;

logic clk;
logic rst;

rra_class my_tester;

rra_interface Round_Robin_Interface(.clk(clk));


SS_WCRRA_4bits uut(

	.clk(clk),
	.rst(rst),
	.reqs(Round_Robin_Interface.reqs),
	.grant(Round_Robin_Interface.grant)

);



initial begin


	rst = 0;
	clk = 0;
	
#10	rst = 1;

	my_tester = new(Round_Robin_Interface);
	

#2	my_tester.req_0();
#2	my_tester.req_1();
#2	my_tester.req_2();
#2	my_tester.req_3();

#50
	$stop;

end

always begin
	#1 clk <= ~clk;
end

endmodule