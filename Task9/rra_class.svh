class rra_class;
	virtual rra_interface RoundRobin_Interface;
	function new (virtual rra_intf.SIMULATION intf);
		RoundRobin_Interface = intf;
		RoundRobin_Interface.reqs = 3'b000;
	endfunction

	task req_0();
		RoundRobin_Interface.reqs[0] = 1'b1; 
	endtask

	task req_1();
		RoundRobin_Interface.reqs[1] = 1'b1; 
	endtask

	task req_2();
		RoundRobin_Interface.reqs[2] = 1'b1; 
	endtask

	task req_3();
		RoundRobin_Interface.reqs[3] = 1'b1; 
	endtask
	
endclass