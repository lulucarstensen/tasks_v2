/*
* Module name: SS_WCRRA_4bits
* Description: This module works as a reference model of
*              a work conserving round robin arbiter of
*              4 bits length
* Author: Santiago Miguel Segovia Cazares
* Date: 29/10/2023
* Version: 1.0
* Release notes:
* 		1.0: Initial version
* Ports:
*  Inputs:
*   clk, clock of the system,
*   rst, reset of the system,
*   reqs, each bit corresponds to a requestor
*  Outputs:
*   grant, each bit corresponds to a requestor
*/
module SS_WCRRA_4bits(
	input clk,
	input rst,
	input [3 : 0] reqs,
	output [3 : 0] grant
);

logic [3 : 0] pending_requests_reg;
logic [3 : 0] pcc_input;
logic [3 : 0] pcc_output;
logic [3 : 0] next_pending_requests_l;
logic [3 : 0] grants_l;

// Next mask reg calculation
always_comb begin
	next_pending_requests_l = (pending_requests_reg == 4'd0)? reqs & (~grants_l) : pending_requests_reg & (~grants_l);
end

// Mask register
always_ff@(posedge clk, negedge rst)begin
	if (!rst) begin
		pending_requests_reg <= 4'd0;
	end
	else begin
		pending_requests_reg <= next_pending_requests_l;
	end
end

// PCC
always_comb begin
	pcc_input = (pending_requests_reg == 4'd0)? reqs : pending_requests_reg;
	pcc_output = { (|pcc_input), (|pcc_input[2:0]), (|pcc_input[1:0]), pcc_input[0]};
end

// Grant calculation
always_comb begin
	grants_l = (pcc_output << 1) ^ pcc_output;
end



assign grant = grants_l;

endmodule
