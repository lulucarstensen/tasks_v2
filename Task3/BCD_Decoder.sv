// BCD Decoder

module BCD_Decoder 
(
	input  [7:0]bin,
	output [6:0]hundreds,
	output [6:0]tens,
	output [6:0]ones
);

wire [11:0]w_bin2bcd_bcd_to_segs;

bin2bcd decoder
(
	.bin_deco(bin),
	.bcd_deco(w_bin2bcd_bcd_to_segs)
);

dec2seg hun_dreds
(
	.bcd_dec2seg(w_bin2bcd_bcd_to_segs[11:8]),
	.segments(hundreds)
);

dec2seg te_ns
(
	.bcd_dec2seg(w_bin2bcd_bcd_to_segs[7:4]),
	.segments(tens)	
);

dec2seg on_es
(
	.bcd_dec2seg(w_bin2bcd_bcd_to_segs[3:0]),
	.segments(ones)
);

endmodule
