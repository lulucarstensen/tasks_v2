// Add 3 if input is bigger or equal than five 

module Add_three 
(
	input [3:0] inp_add, //number compared to check if you need to add three
	output [3:0] out_add //even if there's no addition
);

	assign out_add = (inp_add < 5) ? inp_add : inp_add + 2'd3 ;
	
endmodule 