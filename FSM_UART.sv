// Project: UART Rx
// Module:  FSM
// Author:  María Luisa Ortiz Carstensen
// Date:    23 febrero 2023

module FSM_UART 
(
	input 	clk,
	input 	rst, 
	input 	ovf8,
	input 	ovf5,
	input 	enb,
	input 	rx, 
	
	output 	done,
	output	rx_out
);

typedef enum logic[1:0]{
	IDLE = 2'b00,
	START = 2'b01,
	DATA = 2'b10,
	DONE = 2'b11,

} fsm_type;

fsm_type current_state;
fsm_type next_state;

logic rx_out_l;
logic done_l;

always_comb begin
	case(current_state)
	IDLE: begin //IDLE 
		if(rx == 0)				//Start bit is 0
			next_state = START; 
		else 
			next_state = IDLE;
	end
	START:begin
		if(ovf5 == 1)
		next_state = DATA;
		else
		next_state = START;
	end
	DATA: begin 
		if(ovf8 == 1)
			next_state = PARITY;
		else 
			next_state = DATA;
	end
	DONE:
	default: begin
		next_state = IDLE;
	end
	endcase

end

end module 
