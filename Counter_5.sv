// Project: UART Rx
// Module:  Counter 5
// Author:  María Luisa Ortiz Carstensen
// Date:    23 febrero 2023

module Counter_5 #(parameter DW = 3)
(
	input 	clk, 
	input 	enb,
	input 	rst,
	input		sync_rst,
	
	output	[DW-1:0]count5_o, 
	output 	ovf5
);

logic [DW-1:0]count5_l;
logic ovf5_l;


always_ff @(posedge clk or negedge rst) begin
  if (!rst) begin
		count5_l <= 0;//active on 0
		ovf5_l <= 0;
  end
  else begin
		if (enb) begin
			 if (sync_rst)
				  count5_l <= 0; //active on 1
			 else begin
				  count5_l <= count5_l + 1;
				  if (count5_l == 4) begin
						ovf5_l <= 1;
						count5_l <= 0;
				  end
				  else
						ovf5_l <= 0;
			 end
		end
  end
end

assign count5_o = count5_l;
assign ovf5		 = ovf5_l;

endmodule 







