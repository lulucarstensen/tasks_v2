module Fibonacci(
    input clk,
    input rst,
    output reg [9:0] fib_output
);

// Instancia del módulo Fibonacci
Fibonacci_Beh fib_inst(
    .clk(clk),
    .rst(rst),
    .fib(fib_output)
);

endmodule
